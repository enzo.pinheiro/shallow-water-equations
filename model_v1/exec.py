# -*- coding: utf-8 -*-

import os
import numpy as np
from config import u, v, h, f, g, H, time, dt, dx, dy, nx, ny, forc_h, lon, lon2, lat, lat2, scheme
from shallow_water_model_2D import runge_kutta_4th_scheme_C_2D, leapfrog_scheme_C_2D_ponto
from pyClimateTools.funcoes import create_nc3d

PROJ_DIR = (os.path.dirname(os.path.realpath(__file__))).split('/')[0:-1]
PROJ_DIR = '/'.join(PROJ_DIR)

print ('Integrating the model ...')
if scheme == 'rk-c':
    out_u, out_v, out_h, out_div, out_rot = runge_kutta_4th_scheme_C_2D(u, v, h, f, g, H, time, dt, dx, dy, nx, ny, forc_h)
elif scheme == 'lf-c':
    out_u, out_v, out_h, out_div, out_rot = leapfrog_scheme_C_2D_ponto(u, v, h, f, g, H, time, dt, dx, dy, nx, ny, forc_h)

print ('Saving netcdfs ...')
NC_DIR = PROJ_DIR+'/nc_dir'
if not os.path.exists(NC_DIR):
    os.makedirs(NC_DIR)

create_nc3d(NC_DIR+'/forcing.nc', forc_h, time, lat2, lon2, 'm', 'days since 00-00-0000 00:00', 'f', 'forcing')
create_nc3d(NC_DIR+'/h.nc', np.array(out_h), time, lat2, lon2, 'm', 'days since 00-00-0000 00:00', 'h', 'height')
create_nc3d(NC_DIR+'/u.nc', np.array(out_u), time, lat2, lon, 'm/s', 'days since 00-00-0000 00:00', 'u', 'zonal wind')
create_nc3d(NC_DIR+'/v.nc', np.array(out_v), time, lat, lon2, 'm/s', 'days since 00-00-0000 00:00', 'v', 'meridional wind')

print ('Done')
