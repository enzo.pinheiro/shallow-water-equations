# -*- coding: utf-8 -*-

import numpy as np


def gauss_edecay_factor(var0, nt, nx, ny, xpos, ypos, tpos, nr, nrt):
    """
    Creates a gaussian forcing array with decay factor of e.

    :param var0 (int): Amplitude of the forcing (Maximum at x=xpos, y=ypos and t=tpos)
    :param nt (int): number of time steps
    :param nx (int): number of points at x-coordinate
    :param ny (int): number of points at y-coordinate
    :param xpos (int): position at x-coordinate where forcing magnitude will be maximum in %, i.e. 50 for max magitude at grid's central point
    :param ypos (int): position at y-coordinate where forcing magnitude will be maximum in %, i.e. 50 for max magitude at grid's central point
    :param tpos (int): position at time-coordinate where forcing magnitude will be maximum in %, i.e. 50 for max magitude at grid's central point
    :param nr (int): constant which controls forcing area of influence. Nr = 1 will set forcing to its minimum area of influence. Must be >=1
    :param nrt (int): constant which controls forcing time extent. Nrt = 1 will set forcing to its minimum time extent. Must be  >= 1.

    :return: return forcing 3d array (time, lat, lon)
    """

    out_array = np.zeros((int(nt), int(nx), int(ny)))

    x = np.arange(0, nx)
    y = np.arange(0, ny)
    t = np.arange(0, nt)
    x0 = np.percentile(x, xpos)
    y0 = np.percentile(y, ypos)
    t0 = np.percentile(t, tpos)

    for k in t:
        for i in x:
            for j in y:
                out_array[k, j, i] = var0 * np.exp(-((1/nr*(i - x0))**2) -((1/nr*(j - y0))**2) -((1/nrt*(k - t0))**2))

    return out_array
