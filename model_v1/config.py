# -*- coding: utf-8 -*-

import numpy as np
from gaussian_exp_decay_function import gauss_edecay_factor
from netCDF4 import Dataset


def build_grid(p0, pf, d):

    grid = np.arange(p0 - (d/2.), pf + (3*d/2.), d) # m
    grid2 = np.arange(p0, pf + d, d) # m
    grid_deg = grid/111320.  # deg
    grid_deg2 = grid2/111320.  # deg

    return grid, grid2, grid_deg, grid_deg2


print ('Creating time array ...')
dt = 2000.  # resolução temporal (s)
time = np.arange(dt, 86400*3. + dt, dt)  # matriz do tempo
nt = time.shape[0]  # quantidade de passos no tempo

print ('Creating model grid ...')
dx = 111320.  # resolução espacial em x (m)
x0 = -80.*dx  # x inicial
xf = 0.*dx  # x final
x, x2, lon, lon2 = build_grid(x0, xf, dx)  # construção da grade em x
nx = x.shape[0]  # quantidade de pontos em x

dy = 111320.  # resolução espacial em y (m)
y0 = -40.*dy  # y inicial
yf = 40.*dy  # y final
y, y2, lat, lat2 = build_grid(y0, yf, dy)  # construção da grade em y
ny = y.shape[0]  # quantidade de pontos em y

print ('Setting initial conditions ...')
# Condições iniciais
U = 0  # compo basico de u (m/s)
u = np.zeros((nx-1, ny))  # anomalia de u (m/s)
V = 0  # campo basico de v (m/s)
v = np.zeros((nx, ny-1))  # anomalia de v (m/s)
H = 250.  # campo basico de h (m)
h = np.zeros((nx-1, ny-1))  # anomalia de h (m)

# Constantes 
g = 10.  # constante gravitacional (m/s**2)
d = 1./(600.*((np.pi/100.)**2))  # difusão newtoniana + atrito (m/s)  # 0. para desligar difusao
cfl = np.sqrt(g*H)*dt/dx
print ('CFL = {0}'.format(cfl))

#Coriolis
fo = 0. * y  # f no equador
beta = ((2 * ((2 * np.pi) / 86400) * np.cos(0*(np.pi/180)))/6371000.)  # df/dy no equador
beta_y = beta*y  # beta * y
f = fo + beta_y  # parâmetro de coriolis aproximado pelo plano beta equatorial

# Perturbação no campo da altura
print ('Creating pertubation field ...')
# forc_h = gauss_edecay_factor_2D(0.0001, time, nt, nx-1, ny-1, 10., 500., 0.5, 0.625)
forc_h = gauss_edecay_factor(1, nt, nx-1, ny-1, 50, 50, 50, 10, 400)

# Time-differencing scheme
# Options are: Leapfrog in Arakaw's C grid (lf-c) and Runge-Kutta 4th order in Arakaw's C grid (rk-c), 
scheme = 'rk-c' 
