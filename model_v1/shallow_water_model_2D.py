# -*- coding: utf-8 -*-

import numpy as np
from pyClimateTools.funcoes import rolling_window


def leapfrog_scheme_C_2D(init_u, init_v, init_h, f, g, H, time_array, dt, dx, dy, nx, ny, forc):

    u_array = np.zeros((2, init_u.shape[0], init_u.shape[1]))
    u_array[0, :, :] = init_u
    v_array = np.zeros((2, init_v.shape[0], init_v.shape[1]))
    v_array[0, :, :] = init_v
    h_array = np.zeros((2, init_h.shape[0], init_h.shape[0]))
    h_array[0, :, :] = init_h

    f2 = np.mean(rolling_window(f, 2), 1)
    ci = np.sqrt(g * H)
    cf = -np.sqrt(g * H)

    # FTCS
    fv_boundi = f2 * np.mean(rolling_window(v_array[0, :, 0], 2), 1)
    fv_boundf = f2 * np.mean(rolling_window(v_array[0, :, -1], 2), 1)
    fu_boundi = f[0] * np.mean(rolling_window(u_array[0, 0, :], 2), 1).transpose()
    fu_boundf = f[-1] * np.mean(rolling_window(u_array[0, -1, :], 2), 1).transpose()

    u_boundi = u_array[0, :, 0] + fv_boundi + (np.sqrt(g * H) * (dt / dx) * (u_array[0, :, 1] - u_array[0, :, 0]))
    u_boundf = u_array[0, :, -1] + fv_boundf - (np.sqrt(g * H) * (dt / dx) * (u_array[0, :, -1] - u_array[0, :, -2]))
    v_boundi = v_array[0, 0, :] - fu_boundi + (np.sqrt(g * H) * (dt / dx) * (v_array[0, 1, :] - v_array[0, 0, :]))
    v_boundf = v_array[0, -1, :] - fu_boundf - (np.sqrt(g * H) * (dt / dx) * (v_array[0, -1, :] - v_array[0, -2, :]))

    uterm = np.array([np.mean(np.append(rolling_window(v_array[0, i, :], 2), rolling_window(v_array[0, i + 1, :], 2), 1), 1) * f2[i] for i in np.arange(nx - 1)])
    vterm = np.array([np.mean(np.append(rolling_window(u_array[0, :, i], 2), rolling_window(u_array[0, :, i + 1], 2), 1), 1) * f[1:-1] for i in np.arange(ny - 1)]).transpose()
    ucori = np.concatenate((np.full((nx - 1, 1), np.nan), uterm, np.full((nx - 1, 1), np.nan)), 1)
    vcori = np.concatenate((np.full((1, ny - 1), np.nan), vterm, np.full((1, ny - 1), np.nan)), 0)

    dh_dx = np.array([np.diff(rolling_window(h_array[0, i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
    dh_dx = np.concatenate((np.full((nx - 1, 1), np.nan), dh_dx, np.full((nx - 1, 1), np.nan)), 1)
    dh_dy = np.array([np.diff(rolling_window(h_array[0, :, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()
    dh_dy = np.concatenate((np.full((1, ny - 1), np.nan), dh_dy, np.full((1, ny - 1), np.nan)), 0)
    du_dx = np.array([np.diff(rolling_window(u_array[0, i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
    dv_dy = np.array([np.diff(rolling_window(v_array[0, :, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()

    u_pred = u_array[0, :, :] + (ucori - g * (dh_dx / dx)) * dt
    v_pred = v_array[0, :, :] + (-vcori - g * (dh_dy / dy)) * dt
    h_pred = h_array[0, :, :] + (-H * ((du_dx / dx) + (dv_dy / dy))) * dt

    u_array[1, :, :] = u_pred
    u_array[1, :, 0] = u_boundi
    u_array[1, :, -1] = u_boundf
    v_array[1, :, :] = v_pred
    v_array[1, 0, :] = v_boundi
    v_array[1, -1, :] = v_boundf
    h_array[1, :, :] = h_pred

    time01 = np.zeros(time_array.shape[0], dtype=int)
    time01[::2] = 1

    save_u = []
    save_v = []
    save_h = []
    save_div = []
    save_rot = []

    for m, n in enumerate(time01):
        print (time_array[m])

        uterm = np.array([np.mean(np.append(rolling_window(v_array[n, i, :], 2), rolling_window(v_array[n, i + 1, :], 2), 1), 1) * f2[i] for i in np.arange(nx - 1)])
        vterm = np.array([np.mean(np.append(rolling_window(u_array[n, :, i], 2), rolling_window(u_array[n, :, i + 1], 2), 1), 1) * f[1:-1] for i in np.arange(ny - 1)]).transpose()
        ucori = np.concatenate((np.full((nx - 1, 1), np.nan), uterm, np.full((nx - 1, 1), np.nan)), 1)
        vcori = np.concatenate((np.full((1, ny - 1), np.nan), vterm, np.full((1, ny - 1), np.nan)), 0)

        dh_dx = np.array([np.diff(rolling_window(h_array[n, i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dh_dx = np.concatenate((np.full((nx - 1, 1), np.nan), dh_dx, np.full((nx - 1, 1), np.nan)), 1)
        dh_dy = np.array([np.diff(rolling_window(h_array[n, :, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()
        dh_dy = np.concatenate((np.full((1, ny - 1), np.nan), dh_dy, np.full((1, ny - 1), np.nan)), 0)
        du_dx = np.array([np.diff(rolling_window(u_array[n, i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dv_dy = np.array([np.diff(rolling_window(v_array[n, :, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()

        u_pred = u_array[n - 1, :, :] + (ucori - g * (dh_dx / dx)) * 2 * dt
        v_pred = v_array[n - 1, :, :] + (-vcori - g * (dh_dy / dy)) * 2 * dt
        h_pred = h_array[n - 1, :, :] + (-H * ((du_dx / dx) + (dv_dy / dy)) + forc[m]) * 2 * dt

        # Fronteira
        fv_boundi = f2 * np.mean(rolling_window(v_array[n, :, 0], 2), 1)
        fv_boundf = f2 * np.mean(rolling_window(v_array[n, :, -1], 2), 1)
        fu_boundi = f[0] * np.mean(rolling_window(u_array[n, 0, :], 2), 1).transpose()
        fu_boundf = f[-1] * np.mean(rolling_window(u_array[n, -1, :], 2), 1).transpose()

        u_boundi = u_array[n, :, 0] + fv_boundi + (ci * (dt / dx) * (u_array[n, :, 1] - u_array[n, :, 0]))
        u_boundf = u_array[n, :, -1] + fv_boundf + (cf * (dt / dx) * (u_array[n, :, -1] - u_array[n, :, -2]))
        v_boundi = v_array[n, 0, :] - fu_boundi + (ci * (dt / dx) * (v_array[n, 1, :] - v_array[n, 0, :]))
        v_boundf = v_array[n, -1, :] - fu_boundf + (cf * (dt / dx) * (v_array[n, -1, :] - v_array[n, -2, :]))

        u_pred[:, 0] = u_boundi
        u_pred[:, -1] = u_boundf
        v_pred[0, :] = v_boundi
        v_pred[-1, :] = v_boundf

        # Atualização dos campos
        u_array[n - 1, :, :] = u_pred
        v_array[n - 1, :, :] = v_pred
        h_array[n - 1, :, :] = h_pred

        # Divergente para plotagem
        du_dx2 = np.array([np.diff(rolling_window(u_pred[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dv_dy2 = np.array([np.diff(rolling_window(v_pred[:, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()
        # Rotacional para plotagem
        du_dy2 = np.array([np.diff(rolling_window(u_pred[:, i], 2), axis=1) for i in np.arange(1, ny - 1)]).squeeze().transpose()
        dv_dx2 = np.array([np.diff(rolling_window(v_pred[i, :], 2), axis=1).squeeze() for i in np.arange(1, nx - 1)])
        # Salvar passo no tempo para plotagem
        save_h.append(h_pred)
        save_u.append(u_pred)
        save_v.append(v_pred)
        save_div.append((du_dx2/dx) + (dv_dy2/dy))
        save_rot.append((dv_dx2/dx) - (du_dy2/dy))

    return save_u, save_v, save_h, save_div, save_rot


def leapfrog_scheme_C_2D_ponto(init_u, init_v, init_h, f, g, H, time_array, dt, dx, dy, nx, ny, forc):

    u = np.zeros((2, init_u.shape[0], init_u.shape[1]))
    u[0, :, :] = init_u
    v = np.zeros((2, init_v.shape[0], init_v.shape[1]))
    v[0, :, :] = init_v
    h = np.zeros((2, init_h.shape[0], init_h.shape[0]))
    h[0, :, :] = init_h
    f2 = np.array([((f[i]+f[i-1])/2) for i in np.arange(1, nx)])

    # Fronteira
    for i in np.arange(nx-1):
        u[1, i, 0] = u[0, i, 0] + (f2[i] * ((v[0, i, 0] + v[0, i+1, 0])/2)) + (np.sqrt(g * H) * (dt / dx) * (u[0, i, 1] - u[0, i, 0]))
        u[1, i, -1] = u[0, i, -1] + (f2[i] * ((v[0, i, -1] + v[0, i+1, -1])/2)) - (np.sqrt(g * H) * (dt / dx) * (u[0, i, -1] - u[0, i, -2]))

    for j in np.arange(ny-1):
        v[1, 0, j] = v[0, 0, j] - (f[0] * ((u[0, 0, j] + u[0, 0, j+1])/2)) + (np.sqrt(g * H) * (dt / dx) * (v[0, 1, j] - v[0, 0, j]))
        v[1, -1, j] = v[0, -1, j] - (f[-1] * ((u[0, -1, j] + u[0, -1, j+1])/2)) - (np.sqrt(g * H) * (dt / dx) * (v[0, -1, j] - v[0, -2, j]))

    # Fora da fronteira
    for i in np.arange(nx-1):
        for j in np.arange(1, ny-1):
            fv = ((v[0, i, j]+v[0, i, j-1]+v[0, i+1, j]+v[0, i+1, j-1])/4) * f2[i]
            dh_dx = g*(h[0, i, j]-h[0, i, j-1])/dx
            u[1, i, j] = u[0, i, j] + (fv - dh_dx) * dt

    for i in np.arange(1, nx-1):
        for j in np.arange(ny-1):
            fu = ((u[0, i, j]+u[0, i-1, j]+u[0, i, j+1]+u[0, i-1, j+1])/4) * f[i]
            dh_dy = g*(h[0, i, j]-h[0, i-1, j])/dy
            v[1, i, j] = v[0, i, j] - (fu + dh_dy) * dt

    for i in np.arange(0, nx-1):
        for j in np.arange(0, ny-1):
            du_dx = (u[0, i, j+1] - u[0, i, j])/dx
            dv_dy = (v[0, i+1, j] - v[0, i, j])/dy
            h[1, i, j] = h[0, i, j] - H*(du_dx + dv_dy) * dt

    time01 = np.zeros(time_array.shape[0], dtype=int)
    time01[::2] = 1

    save_u = []
    save_v = []
    save_h = []
    save_div = []
    save_rot = []

    for m, n in enumerate(time01):

        print (time_array[m])

        upred = np.zeros(u[n, :, :].shape)
        vpred = np.zeros(v[n, :, :].shape)
        hpred = np.zeros(h[n, :, :].shape)

        # Fronteira
        for i in np.arange(nx - 1):
            upred[i, 0] = u[n, i, 0] + (f2[i] * np.mean((v[n, i, 0], v[n, i + 1, 0]))) + (np.sqrt(g * H) * (dt / dx) * (u[n, i, 1] - u[n, i, 0]))
            upred[i, -1] = u[n, i, -1] + (f2[i] * np.mean((v[n, i, -1], v[n, i + 1, -1]))) - (np.sqrt(g * H) * (dt / dx) * (u[n, i, -1] - u[n, i, -2]))

        for j in np.arange(ny - 1):
            vpred[0, j] = v[n, 0, j] - (f[0] * np.mean((u[n, 0, j], u[n, 0, j + 1]))) + (np.sqrt(g * H) * (dt / dx) * (v[n, 1, j] - v[n, 0, j]))
            vpred[-1, j] = v[n, -1, j] - (f[-1] * np.mean((u[n, -1, j], u[n, -1, j + 1]))) - (np.sqrt(g * H) * (dt / dx) * (v[n, -1, j] - v[n, -2, j]))

        # Fora da fronteira
        for i in np.arange(nx - 1):
            for j in np.arange(1, ny - 1):  # i = 1 ; j = 2
                fv = f2[i] * ((v[n, i, j] + v[n, i, j-1] + v[n, i+1, j] + v[n, i+1, j-1]) / 4)
                dh_dx = g*(h[n, i, j] - h[n, i, j-1])/(dx)
                upred[i, j] = u[n-1, i, j] + (fv)*2*dt - (dh_dx)*2*dt

        for i in np.arange(1, nx - 1):
            for j in np.arange(ny - 1):
                fu = f[i] * ((u[n, i, j] + u[n, i - 1, j] + u[n, i, j + 1] + u[n, i - 1, j + 1]) / 4)
                dh_dy = g*(h[n, i, j] - h[n, i - 1, j])/(dy)
                vpred[i, j] = v[n-1, i, j] - (fu)*2*dt - (dh_dy)*2*dt

        for i in np.arange(0, nx - 1):
            for j in np.arange(0, ny - 1):
                du_dx = (u[n, i, j + 1] - u[n, i, j])/(dx)
                dv_dy = (v[n, i + 1, j] - v[n, i, j])/(dy)
                hpred[i, j] = h[n-1, i, j] -(H*(du_dx + dv_dy))*2*dt

        hpred = hpred + forc[m]

        u[n-1, :, :] = upred
        v[n-1, :, :] = vpred
        h[n-1, :, :] = hpred

        save_h.append(hpred)
        save_u.append(upred)
        save_v.append(vpred)

    return save_u, save_v, save_h, save_div, save_rot


def runge_kutta_4th_scheme_C_2D(init_u, init_v, init_h, f, g, H, time_array, dt, dx, dy, nx, ny, forc):

    u_array = np.zeros((1, init_u.shape[0], init_u.shape[1]))
    u_array[0, :, :] = init_u
    v_array = np.zeros((1, init_v.shape[0], init_v.shape[1]))
    v_array[0, :, :] = init_v
    h_array = np.zeros((1, init_h.shape[0], init_h.shape[0]))
    h_array[0, :, :] = init_h

    f2 = np.mean(rolling_window(f, 2), 1)

    time0 = np.zeros(time_array.shape[0], dtype=int)

    # TEMPORARIO
    save_u = []
    save_v = []
    save_h = []
    save_div = []
    save_rot = []

    for m, n in enumerate(time0):

        print (time_array[m])

        ks_u = np.zeros((4, init_u.shape[0], init_u.shape[1]))
        ks_v = np.zeros((4, init_v.shape[0], init_v.shape[1]))
        ks_h = np.zeros((4, init_h.shape[0], init_h.shape[1]))

        input_u = np.copy(u_array[n, :, :])
        input_v = np.copy(v_array[n, :, :])
        input_h = np.copy(h_array[n, :, :])

        # Primeira interação parcial
        fv_boundi0 = f2 * np.mean(rolling_window(input_v[:, 0], 2), 1)
        fv_boundf0 = f2 * np.mean(rolling_window(input_v[:, -1], 2), 1)
        fu_boundi0 = f[0] * np.mean(rolling_window(input_u[0, :], 2), 1).transpose()
        fu_boundf0 = f[-1] * np.mean(rolling_window(input_u[-1, :], 2), 1).transpose()

        u_boundi0 = u_array[n, :, 0] + fv_boundi0 + (np.sqrt(g * H) * (dt / dx) * (input_u[:, 1] - input_u[:, 0]))
        u_boundf0 = u_array[n, :, -1] + fv_boundf0 - (np.sqrt(g * H) * (dt / dx) * (input_u[:, -1] - input_u[:, -2]))
        v_boundi0 = v_array[n, 0, :] - fu_boundi0 + (np.sqrt(g * H) * (dt / dx) * (input_v[1, :] - input_v[0, :]))
        v_boundf0 = v_array[n, -1, :] - fu_boundf0 - (np.sqrt(g * H) * (dt / dx) * (input_v[-1, :] - input_v[-2, :]))

        ucori = np.array([np.mean(np.append(rolling_window(input_v[i, :], 2), rolling_window(input_v[i + 1, :], 2), 1), 1) * f2[i] for i in np.arange(nx - 1)])
        vcori = np.array([np.mean(np.append(rolling_window(input_u[:, i], 2), rolling_window(input_u[:, i + 1], 2), 1), 1) * f[1:-1] for i in np.arange(ny - 1)]).transpose()

        dh_dx = np.array([np.diff(rolling_window(input_h[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dh_dy = np.array([np.diff(rolling_window(input_h[:, i], 2), axis=1).squeeze() for i in np.arange(0, nx - 1)]).transpose()
        du_dx = np.array([np.diff(rolling_window(input_u[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dv_dy = np.array([np.diff(rolling_window(input_v[:, i], 2), axis=1).squeeze() for i in np.arange(0, nx - 1)]).transpose()

        ks_u[0, :, 1:-1] = ucori - g*dh_dx/dx
        ks_v[0, 1:-1, :] = -vcori - g*dh_dy/dy
        ks_h[0, :, :] = -H*((du_dx/dx) + (dv_dy/dy))

        input_u0 = np.zeros(input_u[:, :].shape)
        input_u0[:, 0] = u_boundi0
        input_u0[:, -1] = u_boundf0
        input_u0[:, 1:-1] = input_u[:, 1:-1] + (0.5 * dt * ks_u[0, :, 1:-1])

        input_v0 = np.zeros(input_v[:, :].shape)
        input_v0[0, :] = v_boundi0
        input_v0[-1, :] = v_boundf0
        input_v0[1:-1, :] = input_v[1:-1, :] + (0.5 * dt * ks_v[0, 1:-1, :])

        input_h0 = input_h[:, :] + (0.5 * dt * ks_h[0, :, :])

        # Segunda interação parcial
        fv_boundi1 = f2 * np.mean(rolling_window(input_v0[:, 0], 2), 1)
        fv_boundf1 = f2 * np.mean(rolling_window(input_v0[:, -1], 2), 1)
        fu_boundi1 = f[0] * np.mean(rolling_window(input_u0[0, :], 2), 1).transpose()
        fu_boundf1 = f[-1] * np.mean(rolling_window(input_u0[-1, :], 2), 1).transpose()

        u_boundi1 = u_array[n, :, 0] + fv_boundi1 + (np.sqrt(g * H) * (dt / dx) * (input_u0[:, 1] - input_u0[:, 0]))
        u_boundf1 = u_array[n, :, -1] + fv_boundf1 - (np.sqrt(g * H) * (dt / dx) * (input_u0[:, -1] - input_u0[:, -2]))
        v_boundi1 = v_array[n, 0, :] - fu_boundi1 + (np.sqrt(g * H) * (dt / dx) * (input_v0[1, :] - input_v0[0, :]))
        v_boundf1 = v_array[n, -1, :] - fu_boundf1 - (np.sqrt(g * H) * (dt / dx) * (input_v0[-1, :] - input_v0[-2, :]))

        ucori1 = np.array([np.mean(np.append(rolling_window(input_v0[i, :], 2), rolling_window(input_v0[i + 1, :], 2), 1), 1) * f2[i] for i in np.arange(nx - 1)])
        vcori1 = np.array([np.mean(np.append(rolling_window(input_u0[:, i], 2), rolling_window(input_u0[:, i + 1], 2), 1), 1) * f[1:-1] for i in np.arange(ny - 1)]).transpose()

        dh_dx1 = np.array([np.diff(rolling_window(input_h0[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dh_dy1 = np.array([np.diff(rolling_window(input_h0[:, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()
        du_dx1 = np.array([np.diff(rolling_window(input_u0[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dv_dy1 = np.array([np.diff(rolling_window(input_v0[:, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()

        ks_u[1, :, 1:-1] = ucori1 - g*dh_dx1[:, :]/dx
        ks_v[1, 1:-1, :] = -vcori1 - g*dh_dy1[:, :]/dy
        ks_h[1, :, :] = -H*((du_dx1/dx) + (dv_dy1/dy))

        input_u1 = np.zeros(input_u[:, :].shape)
        input_u1[:, 0] = u_boundi1
        input_u1[:, -1] = u_boundf1
        input_u1[:, 1:-1] = input_u[:, 1:-1] + (0.5 * dt * ks_u[1, :, 1:-1])

        input_v1 = np.zeros(input_v[:, :].shape)
        input_v1[0, :] = v_boundi1
        input_v1[-1, :] = v_boundf1
        input_v1[1:-1, :] = input_v[1:-1, :] + (0.5 * dt * ks_v[1, 1:-1, :])

        input_h1 = input_h[:, :] + (0.5 * dt * ks_h[1, :, :])

        # Terceira interação parcial
        fv_boundi2 = f2 * np.mean(rolling_window(input_v1[:, 0], 2), 1)
        fv_boundf2 = f2 * np.mean(rolling_window(input_v1[:, -1], 2), 1)
        fu_boundi2 = f[0] * np.mean(rolling_window(input_u1[0, :], 2), 1).transpose()
        fu_boundf2 = f[-1] * np.mean(rolling_window(input_u1[-1, :], 2), 1).transpose()

        u_boundi2 = u_array[n, :, 0] + fv_boundi2 + (np.sqrt(g * H) * (dt / dx) * (input_u1[:, 1] - input_u1[:, 0]))
        u_boundf2 = u_array[n, :, -1] + fv_boundf2 - (np.sqrt(g * H) * (dt / dx) * (input_u1[:, -1] - input_u1[:, -2]))
        v_boundi2 = v_array[n, 0, :] - fu_boundi2 + (np.sqrt(g * H) * (dt / dx) * (input_v1[1, :] - input_v1[0, :]))
        v_boundf2 = v_array[n, -1, :] - fu_boundf2 - (np.sqrt(g * H) * (dt / dx) * (input_v1[-1, :] - input_v1[-2, :]))

        ucori2 = np.array([np.mean(np.append(rolling_window(input_v1[i, :], 2), rolling_window(input_v1[i + 1, :], 2), 1), 1) * f2[i] for i in np.arange(nx - 1)])
        vcori2 = np.array([np.mean(np.append(rolling_window(input_u1[:, i], 2), rolling_window(input_u1[:, i + 1], 2), 1), 1) * f[1:-1] for i in np.arange(ny - 1)]).transpose()

        dh_dx2 = np.array([np.diff(rolling_window(input_h1[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dh_dy2 = np.array([np.diff(rolling_window(input_h1[:, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()
        du_dx2 = np.array([np.diff(rolling_window(input_u1[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dv_dy2 = np.array([np.diff(rolling_window(input_v1[:, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()

        ks_u[2, :, 1:-1] = ucori2 - g*dh_dx2/dx
        ks_v[2, 1:-1, :] = -vcori2 - g*dh_dy2/dy
        ks_h[2, :, :] = -H*((du_dx2/dx) + (dv_dy2/dy))

        input_u2 = np.zeros(input_u.shape)
        input_u2[:, 0] = u_boundi2
        input_u2[:, -1] = u_boundf2
        input_u2[:, 1:-1] = input_u[:, 1:-1] + (dt * ks_u[2, :, 1:-1])

        input_v2 = np.zeros(input_v.shape)
        input_v2[0, :] = v_boundi2
        input_v2[-1, :] = v_boundf2
        input_v2[1:-1] = input_v[1:-1, :] + (dt * ks_v[2, 1:-1, :])

        input_h2 = input_h[:, :] + (dt * ks_h[2, :, :])

        # Quarta interação parcial
        fv_boundi3 = f2 * np.mean(rolling_window(input_v2[:, 0], 2), 1)
        fv_boundf3 = f2 * np.mean(rolling_window(input_v2[:, -1], 2), 1)
        fu_boundi3 = f[0] * np.mean(rolling_window(input_u2[0, :], 2), 1).transpose()
        fu_boundf3 = f[-1] * np.mean(rolling_window(input_u2[-1, :], 2), 1).transpose()

        u_boundi3 = u_array[n, :, 0] + fv_boundi3 + (np.sqrt(g * H) * (dt / dx) * (input_u2[:, 1] - input_u2[:, 0]))
        u_boundf3 = u_array[n, :, -1] + fv_boundf3 - (np.sqrt(g * H) * (dt / dx) * (input_u2[:, -1] - input_u2[:, -2]))
        v_boundi3 = v_array[n, 0, :] - fu_boundi3 + (np.sqrt(g * H) * (dt / dx) * (input_v2[1, :] - input_v2[0, :]))
        v_boundf3 = v_array[n, -1, :] - fu_boundf3 - (np.sqrt(g * H) * (dt / dx) * (input_v2[-1, :] - input_v2[-2, :]))

        ucori3 = np.array([np.mean(np.append(rolling_window(input_v2[i, :], 2), rolling_window(input_v2[i + 1, :], 2), 1), 1) * f2[i] for i in np.arange(nx - 1)])
        vcori3 = np.array([np.mean(np.append(rolling_window(input_u2[:, i], 2), rolling_window(input_u2[:, i + 1], 2), 1), 1) * f[1:-1] for i in np.arange(ny - 1)]).transpose()

        dh_dx3 = np.array([np.diff(rolling_window(input_h2[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dh_dy3 = np.array([np.diff(rolling_window(input_h2[:, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()
        du_dx3 = np.array([np.diff(rolling_window(input_u2[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dv_dy3 = np.array([np.diff(rolling_window(input_v2[:, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()

        ks_u[3, :, 1:-1] = ucori3 - g*dh_dx3/dx
        ks_v[3, 1:-1, :] = -vcori3 - g*dh_dy3/dy
        ks_h[3, :, :] = -H*((du_dx3/dx) + (dv_dy3/dy))

        ks_u[1, :, :] = ks_u[1, :, :] * 2.
        ks_v[1, :, :] = ks_v[1, :, :] * 2.
        ks_h[1, :, :] = ks_h[1, :, :] * 2.
        ks_u[2, :, :] = ks_u[2, :, :] * 2.
        ks_v[2, :, :] = ks_v[2, :, :] * 2.
        ks_h[2, :, :] = ks_h[2, :, :] * 2.

        u_tend = np.zeros(u_array[n, :, :].shape)
        v_tend = np.zeros(v_array[n, :, :].shape)
        h_tend = np.zeros(h_array[n, :, :].shape)

        u_tend[:, 1:-1] = u_array[n, :, 1:-1] + (dt*1/6)*np.sum(ks_u, 0)[:, 1:-1]
        u_tend[:, 0] = u_boundi0
        u_tend[:, -1] = u_boundf0

        v_tend[1:-1, :] = v_array[n, 1:-1, :] + (dt*1/6)*np.sum(ks_v, 0)[1:-1, :]
        v_tend[0, :] = v_boundi0
        v_tend[-1, :] = v_boundf0

        h_tend[:, :] = h_array[n, :, :] + (dt*1/6)*np.sum(ks_h, 0)[:, :]
        h_tend = h_tend + forc[m]

        u_array[n, :] = u_tend
        v_array[n, :] = v_tend
        h_array[n, :] = h_tend

        # if time_array[m] % 21600 == 0:
        # Divergente
        du_dx2 = np.array([np.diff(rolling_window(u_tend[i, :], 2), axis=1).squeeze() for i in np.arange(0, ny - 1)])
        dv_dy2 = np.array([np.diff(rolling_window(v_tend[:, i], 2), axis=1) for i in np.arange(0, nx - 1)]).squeeze().transpose()
        # Rotacional
        du_dy2 = np.array([np.diff(rolling_window(u_tend[:, i], 2), axis=1) for i in np.arange(1, ny - 1)]).squeeze().transpose()
        dv_dx2 = np.array([np.diff(rolling_window(v_tend[i, :], 2), axis=1).squeeze() for i in np.arange(1, nx - 1)])

        # TEMPORARIO
        save_h.append(h_tend)
        save_u.append(u_tend)
        save_v.append(v_tend)
        save_div.append((du_dx2/dx) + (dv_dy2/dy))
        save_rot.append((dv_dx2/dx) - (du_dy2/dy))

    return save_u, save_v, save_h, save_div, save_rot
