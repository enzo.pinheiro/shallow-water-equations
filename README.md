# shallow-water-equations

Overview:
========
- Shallow water model linearized about a basic state of rest in the equatorial beta plane. Discretizaton of the equations was done through finite difference using the following time-differencing schemes:
    - Leapfrog in Arakawa's C grid
    - Runge-Kutta 4th order in Arakawa's C grid
- Radiation boundary conditions were used
- The forcing can be modified (amplitude and positioning) at ˋ/model_v1/gaussian_exp_decay_function.pyˋ.

How it works:
========
- The model_v1 directory contains the working scripts. To run the model change the model settings (such as horizontal resolution, time step, forcing, time-differencing scheme, etc) at config.py and then invoke:
>>>
$ python exec.py
>>>

- netCDFs outputs are saved in the nc_dir directory
- The model_v2 directory is an improved version of the codes, but stills in development phase.

Example of simulation
========
- An example of a 5 day simulation. Forcing is applied to the height field in the equator. Coloured contours (right colorbar) represent height perturbation and  coloured vectors (left colorbar) are normalized wind perturbation. (Might take a while to load. If it does not load open the example.gif above)

![](example.gif)

Dependencies
========
- python 2 or 3
- numpy
- pyClimateTools (https://gitlab.com/enzo.pinheiro/pyclimatetools)
