# -*- coding: utf-8 -*-

import numpy as np
from pyClimateTools.funcoes import rolling_window


def runge_kutta_4th_scheme_C_2D(init_u, init_v, init_h, f, g, H, time_array, dt, dx, dy, nx, ny, forcing):

    
    def partial_du_dt(u, v, h, f, g, dx, dt, F=0):

        dh_dx = np.diff(h, axis=1)/dx
        v_mean = np.full(dh_dx.shape, np.nan)
        for i in np.arange(1, v.shape[0]):
            for j in np.arange(1, v.shape[1]):
                v_mean[i-1, j-1] = (v[i, j] + v[i-1, j] + v[i, j-1] + v[i-1, j-1])/4
        
        fv = np.full(v_mean.shape, np.nan)
        for j in np.arange(v_mean.shape[1]):
            fv[:, j] = v_mean[:, j]*f
        
        ku = fv - g*dh_dx + F
        u_fore = (ku*dt/3) + u[:, 1:-1]

        return ku, u_fore


    def partial_dv_dt(u, v, h, f, g, dy, dt, F=0):

        dh_dy = np.diff(h, axis=0)/dy
        u_mean = np.full(dh_dy.shape, np.nan)
        for i in np.arange(1, u.shape[0]):
            for j in np.arange(1, u.shape[1]):
                u_mean[i-1, j-1] = (u[i, j] + u[i-1, j] + u[i, j-1] + u[i-1, j-1])/4
        
        fu = np.full(u_mean.shape, np.nan)
        for j in np.arange(u_mean.shape[1]):
            fu[:, j] = u_mean[:, j]*f[1:-1]

        kv = -fu - g*dh_dy + F
        v_fore = (kv*dt/3) + v[1:-1, :]

        return kv, v_fore


    def partial_dh_dt(u, v, h, H, dx, dy, dt, F=0):

        du_dx = np.diff(u, axis=1)/dx
        dv_dy = np.diff(v, axis=0)/dy
        
        kh = -H*(du_dx + dv_dy) + F
        h_fore = (kh*dt/3) + h[:]

        return kh, h_fore
    

    def comp_u_boundaries(u, v, f, g, H, dx, dt):

        fv0 = f * np.mean(rolling_window(v[:, 0], 2), 1)
        fv1 = f * np.mean(rolling_window(v[:, -1], 2), 1)
        u_bound0 = u[:, 0] + fv0 + (np.sqrt(g * H) * (dt / dx) * (u[:, 1] - u[:, 0]))
        u_bound1 = u[:, -1] + fv1 - (np.sqrt(g * H) * (dt / dx) * (u[:, -1] - u[:, -2]))

        return u_bound0, u_bound1
    

    def comp_v_boundaries(u, v, f, g, H, dy, dt):

        fu0 = f[0] * np.mean(rolling_window(u[0, :], 2), 1)
        fu1 = f[-1] * np.mean(rolling_window(u[-1, :], 2), 1)
        v_bound0 = v[0, :] - fu0 + (np.sqrt(g * H) * (dt / dy) * (v[1, :] - v[0, :]))
        v_bound1 = v[-1, :] - fu1 - (np.sqrt(g * H) * (dt / dy) * (v[-1, :] - v[-2, :]))

        return v_bound0, v_bound1
    

    u_array = np.zeros((1, init_u.shape[0], init_u.shape[1]))
    u_array[0, :, :] = init_u
    v_array = np.zeros((1, init_v.shape[0], init_v.shape[1]))
    v_array[0, :, :] = init_v
    h_array = np.zeros((1, init_h.shape[0], init_h.shape[0]))
    h_array[0, :, :] = init_h

    f2 = np.mean(rolling_window(f, 2), 1)

    time0 = np.zeros(time_array.shape[0], dtype=int)

    # TEMPORARIO
    save_u = []
    save_v = []
    save_h = []

    for m, n in enumerate(time0):

        print (time_array[m])

        forc = forcing[m]

        u = u_array[n]
        v = v_array[n]
        h = h_array[n]

        # Primeira interação parcial
        ub01, ub11 = comp_u_boundaries(u, v, f2, g, H, dx, dt)
        vb01, vb11 = comp_v_boundaries(u, v, f, g, H, dy, dt)

        ku1, u_tmp1 = partial_du_dt(u, v, h, f2, g, dx, dt)
        kv1, v_tmp1 = partial_dv_dt(u, v, h, f, g, dy, dt)
        kh1, h_fore1 = partial_dh_dt(u, v, h, H, dx, dy, dt, forc)

        u_fore1 = np.concatenate((ub01[:, np.newaxis], u_tmp1, ub11[:, np.newaxis]), axis=1)
        v_fore1 = np.concatenate((vb01[np.newaxis, :], v_tmp1, vb11[np.newaxis, :]), axis=0)

        # Segunda interação parcial
        ub02, ub12 = comp_u_boundaries(u_fore1, v_fore1, f2, g, H, dx, dt)
        vb02, vb12 = comp_v_boundaries(u_fore1, v_fore1, f, g, H, dy, dt)

        ku2, u_tmp2 = partial_du_dt(u_fore1, v_fore1, h_fore1, f2, g, dx, dt)
        kv2, v_tmp2 = partial_dv_dt(u_fore1, v_fore1, h_fore1, f, g, dy, dt)
        kh2, h_fore2 = partial_dh_dt(u_fore1, v_fore1, h_fore1, H, dx, dy, dt, forc)

        u_fore2 = np.concatenate((ub02[:, np.newaxis], u_tmp2, ub12[:, np.newaxis]), axis=1)
        v_fore2 = np.concatenate((vb02[np.newaxis, :], v_tmp2, vb12[np.newaxis, :]), axis=0)

        # Terceira interação parcial
        ub03, ub13 = comp_u_boundaries(u_fore2, v_fore2, f2, g, H, dx, dt)
        vb03, vb13 = comp_v_boundaries(u_fore2, v_fore2, f, g, H, dy, dt)

        ku3, u_tmp3 = partial_du_dt(u_fore2, v_fore2, h_fore2, f2, g, dx, dt)
        kv3, v_tmp3 = partial_dv_dt(u_fore2, v_fore2, h_fore2, f, g, dy, dt)
        kh3, h_fore3 = partial_dh_dt(u_fore2, v_fore2, h_fore2, H, dx, dy, dt, forc)

        u_fore3 = np.concatenate((ub03[:, np.newaxis], u_tmp3, ub13[:, np.newaxis]), axis=1)
        v_fore3 = np.concatenate((vb03[np.newaxis, :], v_tmp3, vb13[np.newaxis, :]), axis=0)

        # Quarta interação parcial
        ub04, ub14 = comp_u_boundaries(u_fore3, v_fore3, f2, g, H, dx, dt)
        vb04, vb14 = comp_v_boundaries(u_fore3, v_fore3, f, g, H, dy, dt)

        ku4, u_tmp4 = partial_du_dt(u_fore3, v_fore3, h_fore3, f2, g, dx, dt)
        kv4, v_tmp4 = partial_dv_dt(u_fore3, v_fore3, h_fore3, f, g, dy, dt)
        kh4, h_fore4 = partial_dh_dt(u_fore3, v_fore3, h_fore3, H, dx, dy, dt, forc)

        # Calculo da Interacao
        u_tend = np.zeros(u_array[n, :, :].shape)
        v_tend = np.zeros(v_array[n, :, :].shape)
        h_tend = np.zeros(h_array[n, :, :].shape)

        u_tend[:, 1:-1] = u[:, 1:-1] + (dt*1/6)*(ku1 + 2*ku2 + 2*ku3 + ku4)
        u_tend[:, 0] = ub01
        u_tend[:, -1] = ub11

        v_tend[1:-1, :] = v[1:-1, :] + (dt*1/6)*(kv1 + 2*kv2 + 2*kv3 + kv4)
        v_tend[0, :] = vb01
        v_tend[-1, :] = vb11

        h_tend[:, :] = h[:, :] + (dt*1/6)*(kh1 + 2*kh2 + 2*kh3 + kh4)

        u_array[n] = u_tend
        v_array[n] = v_tend
        h_array[n] = h_tend

        # TEMPORARIO
        save_h.append(h_tend)
        save_u.append(u_tend)
        save_v.append(v_tend)

    
    return save_u, save_v, save_h
