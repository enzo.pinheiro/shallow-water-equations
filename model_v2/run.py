# -*- coding: utf-8 -*-

import numpy as np
from config import u, v, h, f, g, H, time, dt, dx, dy, nx, ny, forc_h, lon, lon2, lat, lat2
from shallow_water_model_2D import runge_kutta_4th_scheme_C_2D
from pyClimateTools.funcoes import create_nc3d

print ('Integrating the model ...')
out_u, out_v, out_h = runge_kutta_4th_scheme_C_2D(u, v, h, f, g, H, time, dt, dx, dy, nx, ny, forc_h)

print ('Saving netcdfs ...')
create_nc3d('../output/forcing.nc', forc_h, time, lat2, lon2, 'm', 'days since 00-00-0000 00:00', 'f', 'forcing')
create_nc3d('../output/h.nc', np.array(out_h), time, lat2, lon2, 'm', 'days since 00-00-0000 00:00', 'h', 'height')
create_nc3d('../output/u.nc', np.array(out_u), time, lat2, lon, 'm/s', 'days since 00-00-0000 00:00', 'u', 'zonal wind')
create_nc3d('../output/v.nc', np.array(out_v), time, lat, lon2, 'm/s', 'days since 00-00-0000 00:00', 'v', 'meridional wind')

print ('Done')
